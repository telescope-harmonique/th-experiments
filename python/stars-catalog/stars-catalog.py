#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""process star catalog"""

import json
import os
import math

DATA_PATH = "data"

LST_FILES = [
    "stars.6.json",
    "stars.8.json",
    "stars.14.json",
]


def flatten_star(dic):
    star = {}
    star["coord"] = dic["geometry"]["coordinates"]
    star["id"] = dic["id"]
    star["mag"] = dic["properties"]["mag"]
    star["bv"] = dic["properties"]["bv"]
    return star


def read_catalog(filepath):
    lst_out = []
    if os.path.exists(filepath):
        data_json = json.load(open(filepath, "r"))
        lst = data_json["features"]
        for star in lst:
            lst_out.append(flatten_star(star))
    return lst_out


class StarCatalog:
    def __init__(self, raw_data_path=None):
        self.raw_data_path = raw_data_path
        self.data = []
        if self.raw_data_path:
            self.load_raw_data()

    def load_raw_data(self):
        if os.path.isdir(self.raw_data_path):
            for filename in LST_FILES:
                filepath = os.path.join(self.raw_data_path, filename)
                self.data += read_catalog(filepath)

    def get_dist_point(self, pt, d, mag):
        lst_out = []
        for star in self.data:
            star_dist = math.dist(star["coord"], pt)
            if star_dist < d and star["mag"] < mag:
                lst_out.append(star)
        return lst_out


if __name__ == "__main__":
    cat = StarCatalog(raw_data_path=DATA_PATH)
    print(len(cat.data))
    lst = cat.get_dist_point([50, 50], 10, 7)
    print("len mag 7:", len(lst))
    lst = cat.get_dist_point([50, 50], 10, 9)
    print("len mag 9:", len(lst))
