Catalogue d'étoile
===


Récupérer les étoiles à afficher dans la carte du ciel en les filtrant par position et magnitude.


Objectif final: créer une api


TODO:
- calculer la distance sur une sphère
- trouver un moyen de rendre les calculs plus rapide (numpy...)
