#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Explore Exoplanet.eu_catalog.csv"""

import csv
import json

import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import pandas as pd


# ra, dec = coordonnees
catalog_url = "http://exoplanet.eu/catalog/csv"


field = ['# name',
         'mass',
         'radius',
         'orbital_period',
         'semi_major_axis',
         'discovered',
         'star_name']

ep = []
with open('exoplanet.eu_catalog.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    ep = list(reader)


def star_name(ep_name):
    """
    renvoie le nom de l'étoile à partir du nom de la planète
    """
    if ep_name[-2:-1] == ' ':
        return ep_name[:-2]
    return ep_name

# Rassembler les planètes par système
stars = {}


for p in ep:
    st_name = star_name(p['# name'])
    if st_name in stars:
        stars[st_name].append(p)
    else:
        stars[st_name] = [p]

# Savoir combien de systemes composés de combien de planetes
systems = [[] for i in range(20)]

for st in stars.values():
    systems[len(st)].append(st)


for i in range(10)[1:]:
    print(i, len(systems[i]))

# Ranger les systèmes par nombre de planètes
lst_out = []
for i in range(8, 1, -1):
    lst_out += systems[i]

# exporter en json
json.dump(lst_out, open('systems.json', 'w'), indent=2)

# extraction csv min

exos = pd.read_csv('exoplanet.eu_catalog.csv')
exos[field].to_csv('exoplanet.eu_catalog.sel.csv')


