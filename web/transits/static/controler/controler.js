
window.onload = function(){

    let socket = io();
    console.log("hello hello");
    socket.on('connect', function(){
        console.log("sio connect");
        socket.emit('message', {data: 'hello io'});
    });

    let speed = 1;
    let time_scale = 1;

    let amp_noise = 1;
    let amp_sine = 1;


    let date_min = 0;
    let date_max = 10000;

    let scale = 1;
    let scale_min = 0;
    let scale_max = 1;

    let pos_x = 0;
    let pos_y = 0;


    const lerp = (x, y, a) => x * (1 - a) + y * a;
    const clamp = (a, min = 0, max = 1) => Math.min(max, Math.max(min, a));
    const invlerp = (x, y, a) => clamp((a - x) / (y - x));
    const range = (x1, y1, x2, y2, a) => lerp(x2, y2, invlerp(x1, y1, a));



    function create_slider(selector, min, max, val){
        let new_slider = document.querySelector(selector);
        noUiSlider.create(new_slider, {
            start: [val],
            connect: true,
            step: 0.0001,
            range: {
                'min': min,
                'max': max
            },
        });
        return new_slider;
    }

    let position = 0;
    let zoom = 0;
    const zoom_slider = create_slider(".zoom-slider", 0, 1, 0);
    zoom_slider.noUiSlider.on('update', (v, h) => {
        zoom = v[0];
        update_aladin();
    });

    let zoom_tr = 0;
    let date_position =  5000;
    let date_zoom =  3333;
    const zoom_tr_slider = create_slider(".zoom-tr-slider", 0, 1, 0.8);
    zoom_tr_slider.noUiSlider.on('update', (v, h) => {
        zoom_tr = v[0];
        update_transit();
    });

    const position_slider = create_slider(".position-slider", 0, 1, 0.5);
    position_slider.noUiSlider.on('update', (v, h) => {
        position = v[0];
        update_transit();
    });


    const speed_slider = create_slider(".speed-slider", 0, 10, 1);
    speed_slider.noUiSlider.on('update', (v, h) => {
        speed = v[0];
        update_transit();
    });

    const date_zoom_slider = create_slider(".date-zoom-slider", 0, 10000, 3333);
    date_zoom_slider.noUiSlider.on('update', (v, h) => {
        date_zoom = v[0];
        update_transit();
    });

    const date_position_slider = create_slider(".date-position-slider", 0, 10000, 5000);
    date_position_slider.noUiSlider.on('update', (v, h) => {
        date_position = v[0];
        update_transit();
    });

    const amp_noise_slider = create_slider(".amp-noise-slider", 0.00, 1, 1);
    amp_noise_slider.noUiSlider.on('update', function (values, handle){
        amp_noise = values[0];
        update_transit();
    });

    const amp_sine_slider = create_slider(".amp-sine-slider", 0, 1, 1);
    amp_sine_slider.noUiSlider.on('update', function (values, handle){
        amp_sine = values[0];
        update_transit();
    });

    let touch = false;
    const pad = document.querySelector(".pad");
    const pad_bb = pad.getBoundingClientRect();
    console.log(pad);
    pad.addEventListener("touchstart", function(e){
        e.preventDefault();
        e.stopPropagation();
        touch = true;
        pos_x = ((e.touches[0].clientX - pad_bb.x) / pad_bb.width) * 2 - 1;
        pos_y = ((e.touches[0].clientY - pad_bb.y) / pad_bb.height) * 2 - 1;
    });
    pad.addEventListener("touchmove", function(e){
        e.preventDefault();
        e.stopPropagation();
        touch = true;
        pos_x = ((e.touches[0].clientX - pad_bb.x) / pad_bb.width) * 2 - 1;
        pos_y = ((e.touches[0].clientY - pad_bb.y) / pad_bb.height) * 2 - 1;
    });

    pad.addEventListener("touchend", function(e){
        touch = false;
        pos_x = 0;
        pos_y = 0;
    });

    function update_transit(){
        let data = {zoom_tr: zoom_tr,
                    position: position,
                    speed: speed,
                    date_zoom: date_zoom,
                    date_position: date_position,
                    amp_sine: amp_sine,
                    amp_noise: amp_noise,
                    to: "transit"};
        socket.emit("controler", data=data);
    }
    function update_aladin(){
        let data = {x: pos_x, y: pos_y, zoom: zoom, to: "aladin"};
        socket.emit("controler", data=data);
    }

    setInterval(function(){
        if (touch){
            socket.emit("controler", data={x: pos_x, y: pos_y, to: "aladin"});
        }
    }, 20);
};
