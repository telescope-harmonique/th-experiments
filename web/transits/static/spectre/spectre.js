let height = 500;
let width = 1600;

const margin = {left: 50,
                right: 50,
                top: 50,
                bottom: 50};

const lambd2hue = d3.scaleLinear()
      .domain([0.42, 0.635])
      .range([320.0, 0.0]);

function color(lamb){
    let col = "gray";
    if (lamb < 0.420){
        col = "gray";
    }else if (lamb<0.635){
        let hue = lambd2hue(lamb);
        col = `hsl(${hue}, 100%, 50%)`;

    }else{
        col = "gray";
    }
    return col;
}

const H = 6.62607015e-34;
const C = 299792458;
const K = 1.380649e-23;

const HC2 = 2*H*C*C;
const HC_k = H*C/K;

function planck(T, lamb){
    return (HC2 / (Math.pow(lamb, 5))) * (1/ (Math.exp(H*C/(K*lamb*T))-1));
}
function create_planck_values(temp, n_values=100){
    let val = [];
    for (let i=0; i<n_values; i++){
        let lambd = 10e-9*i;
        let new_val = planck(temp, lambd)/100000000;
        if (isNaN(new_val)) new_val = 0;
        val.push({
            val: new_val,
            lambd: lambd * 1000000
        });
    }
    return val;
}

function create_rainbow(min, max){
    let val = [];
    for (let i=min; i<max; i++){
        let lambd = 10e-9*i;

        val.push({
            val: 100,
            lambd: lambd * 1000000
        });
    }
    return val;
}

let val = [];

let val_rgb = [{col: "red", lambd: 0.620, val: 15},
           {col: "green", lambd: 0.55, val: 52},
           {col: "blue", lambd: 0.47, val: 100},
          ];

let val_plus = [{col: "red", lambd: 0.420, val: 200},
                {col: "red", lambd: 0.620, val: 15},
               {col: "green", lambd: 0.55, val: 52},
               {col: "blue", lambd: 0.47, val: 100},
                {col: "blue", lambd: 0.800, val: 100},
              ];

const rand_val = d3.randomUniform(0, 255);

let rect_width = 30;

const transitions = {
    rgb : function(){
        val = val_rgb;
        x.range([0, 300]);
        rect_width = 50;
        update();
    },
    rainbow : function(){
        val = create_rainbow(42, 63);
        rect_width = 10;
        x.range([0, 300]);
        update();
    },
    jwst : function(){
        val = create_rainbow(0, 200);
        rect_width = 3;
        x.range([0, width - margin.left - margin.right]);
        update();
    },
    plus : function(){
        val = val_plus;
        rect_width = 3;
        x.range([0, 500]);
        update();
    },
    planck : function(){
        val = create_planck_values(3000, 200);
        x.range([0, width - margin.left - margin.right]);
        update();
    }
};

class State{
    constructor(transitions, state){
        this.state = state;
        this.transitions = transitions;
        this.states = Object.keys(this.transitions);
        this.i_state = 0;
    }

    switch(new_state){
        let i_new_state = this.states.indexOf(new_state);
        if (i_new_state >= 0){
            this.i_state = i_new_state;
            this.state = new_state;
            this.transition(new_state);
        }else{
            console.log("state does not exist");
        }
    }

    next(){
        this.i_state = (this.i_state + 1) % this.states.length;
        this.switch(this.states[this.i_state]);
    }

    prev(){
        this.i_state = this.i_state - 1;
        if (this.i_state<0) this.i_state = this.states.length - 1;
        this.switch(this.states[this.i_state]);
    }

    transition(new_state){
        this.transitions[new_state]();
    }
}

const machine = new State(transitions, "rgb");


let y = d3.scaleLinear()
    .domain([0, 1])
    .range([0, height - margin.top - margin.bottom]);

let x = d3.scaleLinear()
    .domain([0, 1])
    .range([0, width - margin.left - margin.right]);

function update(){
    // val.forEach((d) => d.val = rand_val());

    let y_max = 100;

    if (machine.state == "planck"){
        rect_width = 2;
        y_max = d3.max(val, d => d.val);
    }

    let x_min = d3.min(val, d => d.lambd);
    let x_max = d3.max(val, d => d.lambd);

    x.domain([x_min-0.05, x_max+0.05]);
    y.domain([y_max, 0]);

    let t = svg.transition()
          .duration(500);

    bars.selectAll("rect.val")
        .data(val, d => d.lambd)
        .join(
            enter => enter.append("rect")
                .attr("class", "val")
                .attr("x", d => x(d.lambd) - rect_width/2)
                .attr("width", rect_width)
                .attr("y", d => height - margin.bottom - margin.top)
                .attr("height", 0)
                .style("fill", d => d.col ? d.col : "gray")
                .call(enter => enter.transition(t)
                      .attr("width", rect_width)
                      .attr("x", d => x(d.lambd) - rect_width/2)
                      .attr("y", d => y(d.val))
                      .attr("height", d => height - margin.bottom - margin.top - y(d.val))
                      .style("fill", d => color(d.lambd))
                ),
            update => update.call(update => update.transition(t)
                                  .attr("width", rect_width)
                                  .attr("x", d => x(d.lambd) - rect_width/2)
                                  .attr("height", d => height - margin.bottom - margin.top - y(d.val))
                                  .attr("y", d => y(d.val))
                                  .style("fill", d => color(d.lambd))
                                 ),
            exit => exit.call(exit => exit.transition(t)
                                .attr("x", d => x(d.lambd) - rect_width/2)
                                .attr("y", d => height - margin.bottom - margin.top)
                                .attr("height", 0)
                                .style("fill", d => color(d.lambd))
                               )
        );

    y_axis.scale(y);
    y_axis_g.transition(t).call(y_axis)
        .call(g => {
            g.selectAll(".tick line")
                .attr("stroke", "white");
            g.selectAll(".tick text")
                .attr("fill", "white");
        });

    x_axis.scale(x);
    x_axis_g.transition(t).call(x_axis)
        .call(g => {
            g.selectAll(".tick line")
                .attr("stroke", "white");
            g.selectAll(".tick text")
                .attr("fill", "white");
        });

}

const y_axis = d3.axisLeft(y)
      .ticks(5, "r");

const x_axis = d3.axisBottom(x)
      .ticks(10, "r");

const svg = d3.select("body")
      .append("svg")
      .attr("width", width)
      .attr("height", height);

const bars = svg.append("g")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);

const y_axis_g = svg.append("g")
    .attr("transform", `translate(${margin.left}, ${margin.top})`)
    .call(y_axis)
    .call(g => {
        g.selectAll(".tick line")
            .attr("stroke", "white");
        g.selectAll(".tick text")
            .attr("fill", "white");
    });

const x_axis_g = svg.append("g")
      .attr("transform", `translate(${margin.left}, ${height - margin.bottom})`)
      .call(x_axis)
      .call(g => {
          g.selectAll(".tick line")
              .attr("stroke", "white");
          g.selectAll(".tick text")
              .attr("fill", "white");
      });


// let temp = 4000;
// setInterval(()=>{
//     update(temp);
//     temp = 10 + ((temp+10) % 4000);
// }, 100);

svg.on("mousemove", (e) => {
    let x = e.clientX;
    update(x*5);
});

d3.select("body").append("div")
    .attr("class", "th-btn")
    .text("moins")
    .on("click", () => machine.prev());

d3.select("body").append("div")
    .attr("class", "th-btn")
    .text("plus")
    .on("click", () => machine.next());


machine.switch("rgb");
