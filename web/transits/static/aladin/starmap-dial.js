class Dial{
    constructor(elem_parent, width, height, target, starmap){
        this.elem_parent = elem_parent;
        this.width = width;
        this.height = height;
        this.target = target;
        this.starmap = starmap;

        this.changed = true;

        this.canvas = document.createElement("canvas");
        this.canvas.width = width;
        this.canvas.height = height;

        this.elem_parent.append(this.canvas);

        this.ctx = this.canvas.getContext("2d");
        this.ctx.font = "12px arial";
        this.ctx.textAlign = "center";
        this.ctx.fillStyle = "white";

        this.center_x = this.width/2;
        this.center_y = this.height/2;

        this.rad = Math.min(this.width, this.height)/2;
        this.rad_dial = this.rad - 35;
        console.log("[dial] rad", this.rad);
    }

    ra_to_angle(ra){
        return -Math.PI/2.0 + ra*Math.PI/180.0;
    }

    dec_to_angle(dec){
        return -dec*Math.PI/180.0;
    }
}


class DialAsc extends Dial{
    constructor(elem_parent, width, height, target, starmap){
        super(elem_parent, width, height, target, starmap);

        this.cur_target = 7.5*Math.PI/180.0;

        this.draw();
        return this;
    }

    draw_dim(angle, color){
        let angle_moins = this.ra_to_angle( - angle);
        let x_moins = this.center_x + this.rad_dial * Math.cos(angle_moins);
        let y_moins = this.center_y + this.rad_dial * Math.sin(angle_moins);

        let angle_plus = this.ra_to_angle( angle);
        let x_plus = this.center_x + this.rad_dial * Math.cos(angle_plus);
        let y_plus = this.center_y + this.rad_dial * Math.sin(angle_plus);

        this.ctx.strokeStyle = color;
        this.ctx.beginPath();
        this.ctx.moveTo(x_moins, y_moins);
        this.ctx.lineTo(this.center_x, this.center_y);
        this.ctx.lineTo(x_plus, y_plus);
        this.ctx.stroke();
    }

    draw_labels(){
        let label_rad = this.rad-20;

        for (let i = 0; i<24; i++){
            let angle = -Math.PI/2 - Math.PI*i*2/24.0 + this.cur_target;
            let c_angle = Math.cos(angle);
            let s_angle = Math.sin(angle);
            let pos_x = this.center_x + label_rad * c_angle;
            let pos_y = this.center_y + label_rad * s_angle + 5;
            this.ctx.fillText(i + "h", pos_x, pos_y);

            let tick_min = this.rad - 38;
            let tick_max = this.rad - 32;
            let x_tick_min = this.center_x + tick_min*c_angle;
            let y_tick_min = this.center_y + tick_min*s_angle;
            let x_tick_max = this.center_x + tick_max*c_angle;
            let y_tick_max = this.center_y + tick_max*s_angle;

            this.ctx.beginPath();
            this.ctx.moveTo(x_tick_min, y_tick_min);
            this.ctx.lineTo(x_tick_max, y_tick_max);
            this.ctx.stroke();
        }
    }

    draw(){
        if (this.starmap.anim.active || this.changed){
            if (this.starmap.anim.pos_cur[0]){
                this.cur_target = this.starmap.anim.pos_cur[0]*Math.PI/180.0;
            }

            // this.target.target = 7.5;
            // this.target.abs_dim = 10;
            this.ctx.clearRect(0, 0, this.width, this.height);

            this.ctx.strokeStyle = "gray";
            this.ctx.beginPath();
            this.ctx.arc(this.center_x, this.center_y, this.rad_dial, 0, 2*Math.PI);
            this.ctx.stroke();

            this.draw_labels();
            this.draw_dim(7.5+15, "gray");
            this.draw_dim(7.5, "red");
            this.changed = false;
        }
        window.requestAnimationFrame(()=>this.draw());
    }
};

class DialDec extends Dial{
    constructor(elem_parent, width, height, target, starmap){
        super(elem_parent, width, height, target, starmap);

        this.cur_target = 5;
        this.draw();
        return this;
    }

    draw_dim(angle, color){
        let angle_moins = this.dec_to_angle(this.cur_target - angle);
        let x_moins = this.center_x + this.rad_dial * Math.cos(angle_moins);
        let y_moins = this.center_y + this.rad_dial * Math.sin(angle_moins);

        let angle_plus = this.dec_to_angle(this.cur_target + angle);
        let x_plus = this.center_x + this.rad_dial * Math.cos(angle_plus);
        let y_plus = this.center_y + this.rad_dial * Math.sin(angle_plus);

        this.ctx.strokeStyle = color;
        this.ctx.beginPath();
        this.ctx.moveTo(x_moins, y_moins);
        this.ctx.lineTo(this.center_x, this.center_y);
        this.ctx.lineTo(x_plus, y_plus);
        this.ctx.stroke();
    }

    draw_labels(){
        let cur_target = this.starmap.anim.pos_cur[1]*Math.PI/180.0;
        let label_rad = this.rad-20;

        for (let i = 0; i<19; i++){
            let angle = -Math.PI/2 + Math.PI*i/18.0;
            let c_angle = Math.cos(angle);
            let s_angle = Math.sin(angle);
            let pos_x = this.center_x + label_rad * c_angle;
            let pos_y = this.center_y + label_rad * s_angle + 5;
            this.ctx.fillText(Math.abs(- i + 9) + "0°", pos_x, pos_y);

            let tick_min = this.rad - 38;
            let tick_max = this.rad - 32;
            let x_tick_min = this.center_x + tick_min*c_angle;
            let y_tick_min = this.center_y + tick_min*s_angle;
            let x_tick_max = this.center_x + tick_max*c_angle;
            let y_tick_max = this.center_y + tick_max*s_angle;

            this.ctx.beginPath();
            this.ctx.moveTo(x_tick_min, y_tick_min);
            this.ctx.lineTo(x_tick_max, y_tick_max);
            this.ctx.stroke();
        }
    }

    draw(){
        if (this.starmap.anim.active || this.changed){
            if (this.starmap.anim.pos_cur[1]){
                this.cur_target = this.starmap.anim.pos_cur[1];
            }


            // this.target.target = 7.5;
            // this.target.abs_dim = 10;
            this.ctx.clearRect(0, 0, this.width, this.height);

            this.ctx.font = "20px arial";
            this.ctx.fillStyle = "gray";
            this.ctx.fillText("N", this.center_x + this.rad_dial/3, this.center_y - this.rad_dial/3);
            this.ctx.fillText("S", this.center_x + this.rad_dial/3, this.center_y + this.rad_dial/3+20);
            this.ctx.fillStyle = "white";
            this.ctx.font = "12px arial";

            this.ctx.strokeStyle = "gray";
            this.ctx.beginPath();
            this.ctx.arc(this.center_x, this.center_y, this.rad_dial, -Math.PI/2, Math.PI/2);
            this.ctx.lineTo(this.center_x, this.center_y - this.rad_dial);
            this.ctx.stroke();

            this.draw_labels();
            this.draw_dim(5, "red");

            this.ctx.strokeStyle = "gray";
            this.ctx.setLineDash([5, 5]);
            this.ctx.beginPath();
            this.ctx.moveTo(this.center_x, this.center_y);
            this.ctx.lineTo(this.center_x + this.rad_dial, this.center_y);
            this.ctx.stroke();
            this.ctx.setLineDash([]);

            this.changed = false;
        }
        window.requestAnimationFrame(()=>this.draw());
    }
};

export { DialAsc, DialDec }
