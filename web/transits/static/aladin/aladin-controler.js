import * as nav from './aladin-nav.js';

let socket;

let coords = [[],[]];

let coords_ok = [];

let coords_star = [[34, 4, 3, 6, 6, 5, 8, 5, 0, 7],[2, 0, 7, 6, 8, 8, 2, 8, 4]];

coords_star = [[1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1]];

function round_dim(val, dim){
    return Math.round(val / dim) * dim;
}
function floor_dim(val, dim){
    return Math.floor(val / dim) * dim;
}

function list2coord(l){
    let val_s = "";
    l.forEach((val, i) => {
        val_s += val;
        if (i == 1) val_s += ".";
    });
    return parseFloat(val_s);
}


const app = new Vue({
    el: "#app",
    data: {
        coords: [[],[]],
        dim: 0,
        abs_dim: 10,
        next_abs_dim: 1,
        target: {x: 5, y: 5},
        next_target: {x: 5, y: 5},
    },
    watch: {
        dim: function(val){
            this.abs_dim = dim2dim(val);
            this.next_abs_dim = dim2dim(val+1);

            this.update_coords();
        },
    },
    methods: {
        update_coords: function(){
            let x = list2coord(this.coords[0]);
            let y = list2coord(this.coords[1]);

            console.log("update coords", x, y);

            this.target.x = floor_dim(x, this.abs_dim) + this.abs_dim/2;
            this.target.y = floor_dim(y, this.abs_dim) + this.abs_dim/2;

            this.next_target.x = floor_dim(x, this.next_abs_dim/10) + this.next_abs_dim/2;
            this.next_target.y = floor_dim(y, this.next_abs_dim/10) + this.next_abs_dim/2;

            console.log("update target", this.target.x, this.target.y, "next", this.next_target.x, this.next_target.y);
            socket.emit('coords', {data: this.coords});
        }
    }
});

for (let i=0; i<10; i++){
    app.coords[0].push(0);
    app.coords[1].push(0);
    coords[0].push(0);
    coords[1].push(0);
    coords_star[0].push(1);
    coords_star[1].push(1);
    coords_ok.push({0: false, 1: false, 2: false});
}

window.onload = function(){
    socket = io();
    socket.on('connect', function(){
        socket.emit('message', {data: 'hello io'});
    });

    let nav_elem = new nav.Nav(true, app, coords_star);
};
