import * as utils from "../modules/th-utils.js";
import * as commons from "../modules/th-commons.js";

const e = utils.easeInOut(2);

class GridAnim{
    constructor(duration, aladin){
        this.duration = duration;
        this.aladin = aladin;
        this.start = undefined;
        this.active = false;
        this.pos_cur = [undefined, undefined];
        this.fov_cur = undefined;
        this.pos = undefined;
    }

    start_anim(pos_target, fov_target){
        this.start = new Date();

        if (!this.active){
            this.pos_src = this.aladin.getRaDec();
            this.fov_src = utils.max(this.aladin.getFov());
        }else{
            this.pos_src = this.pos_cur;
            this.fov_src = this.fov_cur;
        }

        this.pos_target = pos_target;

        if (Math.abs(this.pos_src[0] - this.pos_target[0]) > 180){
            this.pos_target[0] = - (360 - this.pos_target[0]);
        }

        if (fov_target){
            this.fov_target = fov_target;
        }else{
            this.fov_target = utils.max(this.aladin.getFov());
        }
    }

    anim(){
        let elapsed = new Date() - this.start;
        if (elapsed < this.duration){
            this.active = true;
            let pos_abs = e(elapsed/this.duration);
            this.pos = e(pos_abs);

            this.pos_cur[0] = utils.range(0, 1, this.pos_src[0], this.pos_target[0], this.pos);
            this.pos_cur[1] = utils.range(0, 1, this.pos_src[1], this.pos_target[1], this.pos);
            this.aladin.gotoRaDec(this.pos_cur[0]%360, this.pos_cur[1]);

            this.fov_cur = utils.range(0, 1, this.fov_src, this.fov_target, pos_abs);
            this.aladin.setFov(this.fov_cur);

            return true;
        }else{
            if (this.active){
                this.active = false;
                return true;
            }
        }
        this.start = undefined;
        this.pos = undefined;
        return false;
    }

    get_pos(){
        if (this.active){
            return this.pos;
        }
        return false;
    }
}

// TODO: use dim2res from target

class StarMapDraw{
    constructor(elem, width, height, aladin){

        this.canvas = document.getElementById(elem);
        this.aladin = aladin;
        // this.target = target;

        this.target = [7.5, 5];
        this.anim = new GridAnim(1000, this.aladin);
        this.grid_changed = true;
        this.dim = 0;

        this.abs_dim = 10;

        this.dim2res = commons.dim2res;

        this.abs_dim_x = this.dim2res[this.dim][0];
        this.abs_dim_y = this.dim2res[this.dim][1];

        console.log("anim", this.anim);

        this.width = width;
        this.height = height;

        this.canvas.width = width;
        this.canvas.height = height;

        this.ctx = this.canvas.getContext("2d");
        this.ctx.font = "16px arial";
        this.ctx.textAlign = "center";

        this.sx = (this.width - 400)/2;
        this.sy = (this.height - 400)/2;

        this.cur_big_grid = undefined;
        this.cur_gray_grid = undefined;
        this.old_big_grid = undefined;
        this.init_grid();
    }

    init_grid(){
        // console.log("[draw] aladin corners");
        // for (const pt of this.aladin.getFovCorners(2)){
        //     console.log("corner", pt[0], pt[1]);

        // }
        let big_rect_grid = this.get_rect_for_dim(0);
        this.cur_big_grid = this.get_grid(big_rect_grid, -1);
    }

    change_grid(){
        console.log("[draw] change grid, dim=", this.dim);
        if (this.dim > 1){
            this.old_big_grid = this.cur_big_grid;
            this.cur_big_grid = this.cur_gray_grid;
        }

        let gray_rect_grid = this.get_rect_for_dim(this.dim-1, 1);
        this.cur_gray_grid = this.get_grid(gray_rect_grid);
    }

    draw_grids(){
        // reinit grid on dim 0 to update margin
        if (this.dim == 0 && !this.anim.active){
            this.init_grid();
        }

        // get animation
        let anim_pos = this.anim.get_pos();

        // draw gray grid if dim>0
        if (this.dim >0){
            if (anim_pos){
                let val = utils.range(0, 1, 0.0, 0.5, anim_pos);
                this.ctx.strokeStyle = "rgba(0, 0, 0, "+ val + ")";
            }else{
                this.ctx.strokeStyle = "rgba(0, 0, 0, 0.5)";
            }
            this.draw_grid(this.cur_gray_grid[1]);
        }


        // draw main big grid
        if (anim_pos && this.dim>0){
            let val = utils.range(0, 1, 0.5, 1.0, anim_pos);
            this.ctx.strokeStyle = "rgba(0, 0, 0, "+ val + ")";
        }else{
            this.ctx.strokeStyle = "black";
        }
        this.draw_grid(this.cur_big_grid[1]);

        if (this.old_big_grid){
            this.ctx.strokeStyle = "black";
            this.draw_grid(this.old_big_grid[1]);
        }


        if (this.dim > 0){
            this.draw_target_grid(this.dim);
        }
    }

    get_rect_for_dim(dim, dim_grid=0){
        let res_x = this.dim2res[dim][0];
        let res_y = this.dim2res[dim][1];
        let x = utils.floor_dim(this.target[0], res_x);
        let y = utils.floor_dim(this.target[1], res_y);

        let res_grid_x = res_x;
        let res_grid_y = res_y;
        if (dim_grid!=0){
            res_grid_x = this.dim2res[dim+dim_grid][0];
            res_grid_y = this.dim2res[dim+dim_grid][1];
        }
        return [x+res_x, x, y+res_y, y, res_grid_x, res_grid_y];
    }

    draw_labels(labels){
        this.ctx.clearRect(0, 10, this.width, 25);
        this.ctx.clearRect(10, 0, 40, this.height);
        for (const label of labels){
            this.ctx.fillText(label.val, label.x, label.y);
        }
    }

    get_grid(rect, margin=0){
        let x_min = rect[0];
        let x_max = rect[1];
        let y_min = rect[2];
        let y_max = rect[3];
        let res_x = rect[4];
        let res_y = rect[5];

        if (margin < 0){
            console.log(Math.abs(y_max + y_min)/2);
            if (Math.abs(y_max + y_min)/2 < 45){
                margin = 40;
            }else{
                x_min = 360;
                x_max = 0;
                margin = 0;
            }
            y_min = Math.min(90, y_min + 30);
            y_max = Math.max(-91, y_max - 30);

        }

        let pts = [];
        let labels = [];

        let loc_res_x = res_x;
        let loc_res_y = res_y;
        if (res_y == 10){
            loc_res_x = res_x/10;
            loc_res_y = res_y/10;
        }

        let draw_labels = false;
        if (margin!=0){
            draw_labels = true;
        }

        let margin_max = margin + res_x/100;

        for (let x_pos = utils.floor_dim(x_max - margin, res_x); x_pos <= x_min + margin_max ; x_pos+=res_x){
            let x_label = x_pos;
            let line_y = [];
            let x_cor = x_pos%360;

            if (draw_labels){
                let pt0 = this.aladin.world2pix(x_cor, y_min);
                if (pt0){
                    labels.push({
                        // val: Math.round(x_cor*10000)/10000 +"°",
                        val: utils.dec2sex(x_cor)[0] + "h",
                        y: 30,
                        x: pt0[0]
                    });
                }
            }
            for (let y_pos = utils.floor_dim(y_max-margin, res_y); y_pos <= y_min + margin_max; y_pos+=loc_res_y){
                let pt = [x_cor, y_pos];
                line_y.push(pt);
            }
            pts.push(line_y);
        }

        for (let y_pos = utils.floor_dim(y_max - margin, res_y); y_pos <= y_min + margin_max; y_pos+=res_y){
            if (draw_labels){
                let pt0 = this.aladin.world2pix(x_min, y_pos%90);
                if (pt0){
                    labels.push({val: Math.round(y_pos*10000)/10000 +"°",
                                 x: 30,
                                 y: pt0[1]+7});
                }
            }
            let line_x = [];
            for (let x_pos = utils.floor_dim(x_max - margin, res_x); x_pos <= x_min + margin_max; x_pos+=loc_res_x){
                let x_cor = x_pos%360;
                let pt = [x_cor, y_pos];
                line_x.push(pt);
            }
            pts.push(line_x);
        }
        return [labels, pts];
    }

    draw_grid(pts){
        pts.forEach(l => {
            if (l[0]){
                this.ctx.beginPath();
                let proj = this.aladin.world2pix(l[0][0], l[0][1]);
                if (l[0][1] == 0){
                    this.ctx.setLineDash([10, 10]);
                }
                if (proj){
                    let [x, y] = proj;
                    this.ctx.moveTo(x, y);
                    l.forEach(p => {
                        if (p != null){
                            let proj = this.aladin.world2pix(p[0], p[1]);
                            if (proj){
                                let [x,y] = proj;
                                this.ctx.lineTo(x, y);
                            }
                        }
                    });
                }
                this.ctx.stroke();
                this.ctx.setLineDash([]);
            }
        });
    }

    draw_target_grid(){
        this.ctx.strokeStyle = "cyan";
        this.ctx.lineWidth = 3;
        let next_abs_dim_x = this.dim2res[this.dim][0];
        let next_abs_dim_y = this.dim2res[this.dim][1];

        let [x, y] = this.target;//.get_abs_coords();
        x = utils.floor_dim(x, next_abs_dim_x);
        y = utils.floor_dim(y, next_abs_dim_y);
        let [x_a0, y_a0] = this.aladin.world2pix(x, y);
        let [x_a1, y_a1] = this.aladin.world2pix(x, y+next_abs_dim_y);
        let [x_a2, y_a2] = this.aladin.world2pix(x+next_abs_dim_x, y+next_abs_dim_y);
        let [x_a3, y_a3] = this.aladin.world2pix(x+next_abs_dim_x, y);
        this.ctx.beginPath();
        this.ctx.lineTo(x_a0, y_a0);
        this.ctx.lineTo(x_a1, y_a1);
        this.ctx.lineTo(x_a2, y_a2);
        this.ctx.lineTo(x_a3, y_a3);
        this.ctx.lineTo(x_a0, y_a0);
        this.ctx.stroke();
        this.ctx.lineWidth = 1;
    }

    draw_target(){
        this.ctx.strokeStyle = "black";
        this.ctx.beginPath();
        this.ctx.rect(this.sx, this.sy, 400, 400);
        this.ctx.stroke();

        this.ctx.beginPath();
        this.ctx.moveTo(this.width/2, 0);
        this.ctx.lineTo(this.width/2, this.height);
        this.ctx.moveTo(0, this.height/2);
        this.ctx.lineTo(this.width, this.height/2);
        this.ctx.stroke();

        this.ctx.beginPath();
        this.ctx.moveTo(this.sx, this.height/2);
        this.ctx.lineTo(this.sx + 400, this.height/2);
        this.ctx.moveTo(this.width/2, this.sy);
        this.ctx.lineTo(this.width/2, this.sy+400);
        this.ctx.stroke();
    }

    draw_sensor(){
        this.ctx.strokeStyle = "yellow";
        this.ctx.beginPath();
        let [x, y] = this.aladin.getRaDec();
        let [x_a0, y_a0] = this.aladin.world2pix(x - sensor[0], y - sensor[1]);
        let [x_a1, y_a1] = this.aladin.world2pix(x + sensor[0], y - sensor[1]);
        let [x_a2, y_a2] = this.aladin.world2pix(x + sensor[0], y + sensor[1]);
        let [x_a3, y_a3] = this.aladin.world2pix(x - sensor[0], y + sensor[1]);
        this.ctx.beginPath();
        this.ctx.lineTo(x_a0, y_a0);
        this.ctx.lineTo(x_a1, y_a1);
        this.ctx.lineTo(x_a2, y_a2);
        this.ctx.lineTo(x_a3, y_a3);
        this.ctx.lineTo(x_a0, y_a0);
        this.ctx.stroke();
        this.ctx.drawImage(this.aladin_canvas, x_a0, y_a0, x_a1 - x_a0, y_a2 - y_a1, x_a0, y_a0,  x_a1 - x_a0, y_a2 - y_a1);
        this.ctx.strokeStyle = "black";
    }

    draw_debug(){
        this.ctx.strokeStyle = "blue";
        let [x,y] = this.aladin.world2pix(this.target[0], this.target[1]);
        this.ctx.rect(x-5, y-5, 10, 10);
        this.ctx.stroke();
    }

    draw(){
        if (this.anim.anim()) this.grid_changed = true;

        if (this.grid_changed){
            this.ctx.clearRect(0, 0, this.width, this.height);
            this.draw_grids();
            // this.draw_target();
            // this.draw_debug();
            this.grid_changed = false;
        }

        // if (send_osc){
        //     get_col();
        // }

        window.requestAnimationFrame(()=>this.draw());
    }
}

export { StarMapDraw }
