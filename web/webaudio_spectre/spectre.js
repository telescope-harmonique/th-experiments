console.log("hello");

window.onload = function(){
    let audioContext = new (window.AudioContext || window.webkitAudioContext)();
    let osc_list = [];

    let n_osc = 10;

    let masterGainNode = null;

    let pitch = 60;
    let cpt = 0;

    class Osc{
        constructor(freq, num){
            console.log("create Osc");
            this.num = num;
            this.mul = 1;
            this.gain = audioContext.createGain();
            this.gain.connect(audioContext.destination);
            this.gain.gain.value = 0.1;
            this.osc = audioContext.createOscillator();
            this.pitch(freq);
            this.osc.connect(this.gain);
            this.osc.type = "triangle";
            this.osc.start();
        }

        volume(vol){
            this.gain.gain.value = vol;
        }

        pitch(freq){
            this.freq = freq;
            this.osc.frequency.value = this.freq * (this.num + 1) * this.mul;
        }

        ch_mul(mul){
            this.mul = mul;
            this.osc.frequency.value = this.freq * (this.num + 1) * this.mul;
        }

        kill(){
            this.gain.disconnect();
            this.osc.disconnect();
        }
    }


    let volume_control = document.querySelector("input[name='volume']");
    volume_control.addEventListener("input", change_volume, false);

    let pitch_control = document.querySelector("input[name='pitch']");
    pitch_control.addEventListener("input", change_pitch, false);

    let mul_control = document.querySelector("input[name='mul']");
    mul_control.addEventListener("input", change_mul, false);

    function change_volume(){
        osc_list.forEach(o => o.volume(volume_control.value));
    };
    function change_pitch(){
        osc_list.forEach(o => o.pitch(pitch_control.value));
    };
    function change_mul(){
        osc_list.forEach(o => o.ch_mul(mul_control.value));
    };


    document.querySelector("#play").onclick = function(){
        osc_list.push(new Osc(pitch, ++cpt));
    };

    document.querySelector("#stop").onclick = function(){
        osc_list.forEach(o => o.kill());
        cpt = 0;
        osc_list = [];
    };
    // document.querySelector("
};


