import * as d3 from "d3";
import './style.css';
// import css from 'style.css';

let planets = [];
let systems = {};

let width = window.innerWidth;
let height = window.innerHeight;

let play = true;

d3.csv('exoplanet.eu_catalog.sel.csv')
    .then(data => process(data));

function process(data){
    console.log('data', data);
    planets = data;
    window.planets = planets;
    planets.forEach(p => {
        let star_name = p.star_name.trim();
        if (star_name in systems){
            systems[star_name].planets.push(p);
        }else{
            systems[star_name] = {'planets': [p]};
        }

        if (p.semi_major_axis != ''){
            p.orbit = parseFloat(p.semi_major_axis);
        }else{
            p.orbit = 0;
        }
        // p.value = p.orbit;
        p.angle = d3.randomUniform(0, 2*Math.PI)();
        p.period = 0;
        if (p.orbital_period != ''){
            p.period = parseFloat(p.orbital_period);
        }
    });
    console.log('systems', systems);

    let by_size = {};
    let i = 0;

    let systems_l = [];

    Object.entries(systems).forEach(([k, s]) => {
        if (k != ''){
            let n = s.planets.length;
            if (n in by_size){
                by_size[n].push(s);
            }else{
                by_size[n] = [s];
                i += 1;
            }
        }
    });

    let tot_planets = 0;
    function select_systems(n){
        Object.entries(systems).forEach(([k, s]) => {
            if (k != ''){
                if (s.planets.length >= n){ 
                    let o_max = d3.max(s.planets, d => d.orbit);
                    if (o_max > 0){
                        s.o_max = o_max;
                        systems_l.push({o_max: o_max, value: 2*Math.PI*o_max*o_max, name: k});
                        tot_planets += s.planets.length;
                    }
                }
            }
        });
    }

    select_systems(2);

    console.log('select systems', systems_l);

    console.log('by_size', by_size);

    let by_size_l = [];
    Object.entries(by_size).forEach(([n, l]) => {
        l.forEach( s => {
            by_size_l.push(s);
        });
    });

    by_size_l.reverse();
    console.log('by_size_l', by_size_l);


    let num = 21;

    let by_size_p = [];
    by_size_l.forEach( (d, i) => {
        let sys = systems[d.planets[0].star_name.trim()];
        sys.pos = i;
        sys.x = i % num * 1600/(num+1) + 1600/(num+1)/2;
        sys.y = parseInt(i / num) * 1600/(num+1) + 1600/(num+1)/2;
        by_size_p = by_size_p.concat(d.planets);
    });

    let pad = 4;
    let plapack = d3.pack()
        .size([width,height])
        .padding(0)(d3.hierarchy({value: 1, children: systems_l})
                    .sum(d => d.value)
                    // .sort(function comparator(a, b){
                    //     return a.value - b.value;
                    // })
                   );

    console.log('plapack', plapack);

    plapack.children.forEach(d => {
        systems[d.data.name].pack = d;
    });

    d3.select('body').append('canvas')
        .attr('id', 'canvas')
        .attr('width', width)
        .attr('height', height);


    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');
    ctx.imageSmoothingEnabled = true;
    ctx.font = "12pt Lato";

    let ratio = plapack.children[0].r / plapack.children[0].data.o_max;
    console.log(plapack.children[0].r, plapack.children[0].data.o_max);
    console.log(ratio);


    let base_width = 1.4;
    let time_scale = 1/100;

    let time_scale_scale = d3.scalePow()
        .exponent(2)
        .domain([1, 200])
        .range([1/50, 50]);

    let current_transform = {x: 0, y: 0, k: 1};


    let cpt_pla = 0;
    let to_draw = [];
    by_size_p.forEach(p => {
        let sys = systems[p.star_name.trim()];
        if (sys.pack != undefined){
            if (p.orbit == 0){
                // console.log('no orbit', p);
            }else{
                cpt_pla +=1;
                to_draw.push(p);
            }
            let r = sys.pack.r;
            p.sys = sys;
            p.x = sys.pack.x; 
            p.y = sys.pack.y;
        }
    });
    console.log(cpt_pla);

    function process_orbits(){
        to_draw.forEach(p => {
            let r = p.orbit * ratio;
            let rr = (p.sys.o_max * ratio - (pad/2.0)/current_transform.k) / (p.sys.o_max*ratio);
            if (rr > 0){
                r = r*rr;
            }else{
                r = 0;
            }
            p.r = r;
        });
    }

    process_orbits();

    function anim_canvas(){


        function circle(x, y, r){
            ctx.beginPath();
            ctx.ellipse(x, y, r, r, 0, 0, 2 * Math.PI);
            ctx.stroke();
        }

        function line(x, y, r, a){
            ctx.beginPath();
            ctx.moveTo(x, y);
            ctx.lineTo(x + r * Math.cos(a), y + r * Math.sin(a));
            ctx.stroke();
        }

        let cpt = 0;
        let last_elaps = 0;
        function draw_pack(elapsed){
            time_scale = time_scale_scale(current_transform.k);
            let del = elapsed - last_elaps;
            last_elaps = elapsed;
            if (play){
                // current_transform.k =  0.05 + Math.pow((1 + Math.sin(elapsed / 2000.0))/2, 3)*300 ;
                // process_orbits();
                // current_transform.x = (width/2) - 0.4121*width * current_transform.k;
                // current_transform.y = (height/2) - 0.7691*height * current_transform.k;
                // current_transform.x = width/2 - width*22.2981*current_transform.k;
                // current_transform.y = height/2 - height*16.2035*current_transform.k;

                ctx.save();
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.translate(current_transform.x, current_transform.y);
                // ctx.scale(current_transform.k, current_transform.k);
                let line_w = base_width / current_transform.k;
                ctx.lineWidth = 1;// / current_transform.k;
                let s = current_transform.k;
                to_draw.forEach(p => {
                    circle(p.x*s, p.y*s, p.r*s);
                    line(p.x*s, p.y*s, p.r*s, p.angle);
                    if (p.period > 0){
                        p.angle += del/p.period/time_scale;
                    }
                });
                ctx.restore();

                // draw scale
                ctx.fillText((1000/del).toPrecision(5) + " fps", 30, height-60); 
                ctx.fillText(((0.5/ratio) * 200/current_transform.k).toPrecision(4) + " UA", 30, height-30); 
                line(20, height-20, 200, 0);
                line(20, height-10, 20, -Math.PI/2);
                line(220, height-10, 20, -Math.PI/2);
                if (cpt%20 == 0){
                    console.log(line_w);
                }
                // cpt += 1;
            }
        }

        d3.interval(draw_pack, 50);

        if (false){
            function draw_canvas(){
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                by_size_p.slice(0, 4000).forEach(p => {
                    let x = systems[p.star_name.trim()].x;
                    let y = systems[p.star_name.trim()].y;
                    circle(x, y, p.orbit*100);
                    line(x, y, p.orbit*100, p.angle);

                    if (p.period > 0){
                        p.angle += 100/p.period;
                    }
                });
            }

            d3.timer(draw_canvas);
        }
    }


    anim_canvas();

    function anim_svg(){
        let svg = d3.select('body').append('svg')
            .attr('width', window.innerWidth)
            .attr('height', 15000);

        let s_planets = svg.selectAll('g')
            .data(by_size_p.slice(0, 500))
            .enter()
            .append('g')
            .attr('transform', d => {
                let x = systems[d.star_name.trim()].x;
                let y = systems[d.star_name.trim()].y;
                return 'translate(' + x + ', ' + y + ')';
            });

        let planets_g = s_planets.append('g');

        planets_g.append('circle')
            .attr('fill', 'transparent')
            .attr('stroke', 'black')
            .attr('cx', 0)
            .attr('cy', 0)
            .attr('r', d => d.orbit*100);

        planets_g.append('line')
            .attr('stroke', 'black')
            .attr('x1', 0)
            .attr('y1', 0)
            .attr('x2', d => d.orbit*100)
            .attr('y2', 0);

        d3.timer(() => {
            planets_g.attr('transform', p => {
                p.angle += 100/p.period;
                return 'rotate(' + p.angle + ')';
            });
        });
    }



    d3.select('canvas').call(d3.zoom().on("zoom", zoomed));//.on("wheel.zoom", wheeled);

    d3.select('canvas').on('click', () => play = !play);

    function zoomed(){
        if (play){
            current_transform = d3.event.transform;
            process_orbits();
        }
    }

    const panel = d3.select('body').append('div')
          .attr('class', 'panel');

    panel.append('h2')
        .text('Zoomable extra solar systems packing');
    panel.append('h3')
        .text('Extra solar systems with two and more planets');

    panel.append('p').text('Each exoplanet is represented by its orbit (whithout eccentricity).');
    panel.append('p').text('Time scale changes with zoom.');

    panel.append('p').text('(' + cpt_pla + ' exoplanets in ' + systems_l.length + ' systems)');

    //anim_svg();
}
