boolean record = false;


JSONArray systems;
float time_ratio = 100;
float size_ratio = 1.0;

float rad_max_all = 0.0;

// float scales[] = {500, 200, 100, 50, 20, 10, 2, 1, 0.5};
float scales[] = {1000, 100, 10, 2, 0.5};
int scale_pos = 1;
float past_scale = scales[0];
float cur_scale = scales[0];
float next_scale = scales[1];
int dir_scale = 1;

int num_frames = 30;
int pos_frame = 0;

long UA = 149597;


float frame_start = 100000;
/**
 * Animation helper
 */
float ease(float p) {
  return 3*p*p - 2*p*p*p;
}

float ease(float p, float g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}


/**
 * Get the maximale radius of a system
 */
float get_max_radius(JSONArray system){
  float rad_max = 0;
  for (int i = 0; i<system.size();i++){
    JSONObject planet = system.getJSONObject(i);
    noFill();
    String radius_s = planet.getString("semi_major_axis");
    float radius = float(radius_s);
    if (radius == radius){
      rad_max = max(rad_max, radius);
    }
  }
  return rad_max;
}


/**
 * find missing data
 * create fake data if missing and add a flag to remember that is fake
 */
void find_missing(JSONArray systems){
  for (int i = 0; i<systems.size(); i++){
    JSONArray system = systems.getJSONArray(i);

    // print planet name
    String planet_name = system.getJSONObject(0).getString("# name");
    println("fix missing for: ", planet_name);

    // create fake radius
    float rad_max = get_max_radius(system);
    rad_max_all = max(rad_max_all, rad_max);
    float rad = rad_max/2;      // if not all orbit are missing, fake radius is half max radius
    if (rad == 0){              // else fake_radius is 1
      rad = 1;
    }
    for (int j = 0; j < system.size(); j++){
      JSONObject planet = system.getJSONObject(j);
      String orbit_s = planet.getString("semi_major_axis");
      float orbit = float(orbit_s);
      if (orbit != orbit){
        // println("no semi major");
        planet.setString("semi_major_axis", str(100.0));
        planet.setInt("had_radius", 0);
        rad = rad*0.7;          // change fake radius if other planets without orbit
        // println(rad);
      }else{
        planet.setInt("had_radius", 1);
      }


      String period_s = planet.getString("orbital_period");
      float period = float(period_s);
      if (period != period){
        println("no period");
        planet.setBoolean("had_period", false);
        planet.setString("orbital_period", "0");
      }else{
        planet.setBoolean("had_period", true);
      }
    }
  }
}

void setup(){
  size(1000, 1000);
  systems = loadJSONArray("data/systems.json");
  find_missing(systems);
  println("max rad: ", rad_max_all);
  println("scale_pos: ", scale_pos);
  println("next_scale: ", next_scale);
  println("dir_scale: ", dir_scale);
}

void draw(){
  background(0);
  int n = 15;                   // system by row/column
  time_ratio = 0.7;              // time scale for animation

  // int n = int(map(mouseX, 0, width, 1, 20));
  // time_ratio = map(mouseY, 0, height, 1, 1000);
  // size_ratio = map(mouseX, 0, width, 0.01, 10);

  float w = 0;
  if (n > 0){
    w = float(width)/n;
  }
  cur_scale = map(ease(float(pos_frame)/num_frames, 4), 0, 1, past_scale, next_scale);

  size_ratio = map(cur_scale, 0, (n-3)*w, 0, w-10);

  // draw_scale(n, w);;
  draw_solar_sytem(width/2, height/2, n, w);


  for (int i=0; i < n; i++){
    for (int j=0; j < n; j++){
      if (i == 7 && j == 7){
        
      }else{
        JSONArray system = systems.getJSONArray(i + j*n);
        draw_system(system, i*w, j*w, w, w);
      }
    }
  }

  pos_frame = (frameCount + 1) % num_frames;
  if (pos_frame == 0){
    scale_pos += dir_scale;
    if (dir_scale == 1){
      past_scale = scales[scale_pos - 1];
    }else{
      past_scale = scales[scale_pos + 1];
    }
    next_scale = scales[scale_pos];
    if (scale_pos == scales.length-1){
      dir_scale = -dir_scale;
    }
    if (dir_scale == -1 && scale_pos == 0){
      dir_scale = 1;
    }
    println("scale_pos: ", scale_pos);
    println("next_scale: ", next_scale);
    println("dir_scale: ", dir_scale);
  }

  if (record && frameCount < (scales.length-1)*2*num_frames){
    saveFrame("frame_####.png");
  }else{
    record = false;
  }
}

void draw_solar_sytem(float x, float y, int n, float w){

  pushMatrix();
  translate(x, y);


  boolean draw_label = false;

  float pos_1UA = map(1, 0, size_ratio, 0, w-10);
  float pos = (n-3)*w;

  stroke(0, 0, 255);
  pushMatrix();
  rotate((frame_start + frameCount)/(365*time_ratio));
  line(0, 0, pos_1UA, 0);

  popMatrix();
  noFill();
  ellipse(0, 0, pos_1UA*2, pos_1UA*2);

  fill(0, 0, 255);
  if (pos_1UA < pos - 200 && pos_1UA > 100){
    ellipse(pos_1UA, 0, 10, 10);
    if (draw_label){
      text("Earth", pos_1UA+10, 20);
      text("1 UA", pos_1UA+10, -9);
    }
  }

  fill(255);
  stroke(255);

  float planet_val[] = {30, 19.6, 9.5, 5.2, 1.5, 0.7, 0.4};
  float planet_per[] = {165*365, 84*365, 29*365, 12*365, 687, 225, 88};
  String planet_name[] = {"Neptune", "Uranus", "Saturn", "Jupiter", "Mars", "Venus", "Mercury"};

  for (int i=0; i<7; i++){
    float pos_val = pos_1UA * planet_val[i];
    // strokeWeight(2);
    strokeWeight(1);
    noFill();
    ellipse(0, 0, pos_val*2, pos_val*2);

    pushMatrix();
    rotate((frame_start + frameCount)/(planet_per[i]*time_ratio));
    stroke(0,255,0);
    line(0, 0, pos_val, 0);

    popMatrix();
    stroke(255);
    if (pos_val < pos - 200 && pos_val > 100 && draw_label){

      fill(255);
      //ellipse(pos_val, 0, 10, 10);
      noFill();
      text(planet_name[i],pos_val + 10, 20);
      text(str(planet_val[i]) + " UA", pos_val+10, -9);
      // line(pos_val, -10, pos_val, 10);
    }
  }
  fill(200, 200, 0);
  noStroke();
  ellipse(0, 0, 5, 5);
  popMatrix();
}

void draw_scale(int n, float w){

  textSize(15);

  pushMatrix();
  translate(0, (n-1)*w);

  pushMatrix();
  translate(w/2, w/2);
  fill(255, 255, 0);
  stroke(255, 255, 0);
  strokeWeight(2);
  ellipse(0, 0, 20, 20);
  float pos = (n-3)*w;
  stroke(255);
  fill(255);
  line(0, 0, pos, 0);

  float big_scale = map(pos, 0, (w-10), 0, size_ratio);
  long big_scale_km = int(big_scale * UA);
  strokeWeight(3);
  line(pos, -10, pos, 10);
  textAlign(RIGHT);
  text(nf(big_scale, 0, 2).replace(',', '.') + " UA", pos - 9, -9);
  text(nfc(round(big_scale_km)) + " 000 km", pos - 9, 20);

  textAlign(LEFT);

  draw_solar_sytem(0, 0, n, w);
  // float scale_val[] = {0.5, 2, 5, 10, 20, 50};
  // for (int i=0; i<6; i++){
  //   float pos_val = pos_1UA * scale_val[i];
  //   noFill();
  //   // ellipse(0, 0, pos_val*2, pos_val*2);
  //   if (pos_val < pos - 50 && pos_val > 100){
  //     // line(pos_val, 0, pos_val, 10);
  //     // text(str(scale_val[i]) + " UA", pos_val+5, 15);
  //   }
  // }


  translate((n-2)*w, 0);
  stroke(255, 0, 0);
  ellipse(0, 0, w-10, w-10);

  strokeWeight(1);
  line(-(w-10)/2, 0, (w-10)/2, 0);
  line(-(w-10)/2, 0, -(w-10)/2 + 10, 10);
  line(-(w-10)/2, 0, -(w-10)/2 + 10, -10);
  line((w-10)/2, 0, (w-10)/2 - 10, 10);
  line((w-10)/2, 0, (w-10)/2 - 10, -10);

  fill(255, 0, 0);
  text(nf(size_ratio, 0, 2) + " UA", -30, -5);

  translate(w, 0);
  textAlign(RIGHT);
  fill(255);
  text("At scale", w/2 - 5, 10);

  fill(255, 0, 0);
  text("Out of scale", w/2 - 5, 30);

  fill(80);
  text("Missing data", w/2 - 5, 50);
  popMatrix();
  popMatrix();
}

void draw_system(JSONArray system, float x, float y, float w, float h){
  pushMatrix();

  translate(x+w/2, y+h/2);


  float radius_max = get_max_radius(system);


  float radius_max_scale = map(radius_max, 0, size_ratio, 0, w-10);
  radius_max_scale = min(radius_max_scale, w-10);
  if (radius_max_scale > 2){
    noStroke();
    fill(0, 150);
    ellipse(0,0, radius_max_scale, radius_max_scale);
  }
  float max_loc_radius = 0;

  boolean has_overscale_orbit = false;

  for (int i = 0; i<system.size();i++){
    JSONObject planet = system.getJSONObject(i);
    noFill();
    stroke(255);

    String radius_s = planet.getString("semi_major_axis");

    // get orbit as float and normalize with max orbit
    // float radius = map(float(radius_s), 0, radius_max, 0, w-10);
    float radius = map(float(radius_s), 0, size_ratio, 0, w-10);
    max_loc_radius = max(radius, max_loc_radius);

    // ensure radius in not NaN and change style
    // if it was unknown
    if (radius == radius && radius > 2){
      strokeWeight(1);
      if (planet.getInt("had_radius")==1){
        stroke(255);
      }else{
        strokeWeight(2);
        stroke(50);
        radius = w-10;
      }

      // draw orbit
      if (radius <= w-10){
        ellipse(0, 0, radius , radius );
      // }else if (radius < 2*w){
      //   strokeWeight(0.5);
      //   stroke(200);
      //   ellipse(0, 0, radius, radius);
      //   strokeWeight(1);
      //   stroke(255, 0, 0);
      //   ellipse(0, 0, w-10, w-10);
      }else{
        // fill(50, 100);
        stroke(255, 0, 0);
        ellipse(0, 0, w-10, w-10);
        has_overscale_orbit = true;
      }
    }

    pushMatrix();
    float per = float(planet.getString("orbital_period"));

    // ensure period in not NaN and change style
    // if it was unknown
    if (per == per && radius > 2){
      if (per!=0){
        rotate((frame_start + frameCount)/(per*time_ratio));
      }

      if (true){ // draw as line
        strokeWeight(2);
        if (planet.getInt("had_radius") == 1){
          // stroke(0, 200, 0);
          if (radius > w-10){
            stroke(200, 0, 0);
          }else{
            stroke(0, 200, 0);
          }
        }else{
          strokeWeight(2);
          stroke(50);
        }
        // if (radius < 2*w){
        line(min(radius/2, (w-10)/2), 0, 0, 0);
        // }
      }else{     // draw as point
        noStroke();
        fill(0, 200, 0);
        ellipse(radius/2, 0, 5, 5);
      }
    }
    popMatrix();
  }

  if (max_loc_radius > 2){
    fill(200, 200, 0);
    noStroke();
    ellipse(0, 0, 5, 5);
  }
  if (has_overscale_orbit){
    noFill();
    strokeWeight(2);
    stroke(255, 0, 0);
    ellipse(0, 0, w-10, w-10);
  }
  popMatrix();
}

