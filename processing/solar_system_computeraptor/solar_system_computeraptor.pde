/*
The Solar System Simulator - I
ComputeRaptor.com
Syed I. Tauhidi
tuto: https://www.instructables.com/id/Solar-System-Simulator-using-Processing/ 
code : https://gist.github.com/ibtisamtauhidi/68f5fee3e1e0a00ec244 
*/

import java.util.Date;

// Eccentricity, aphelion (semi-major axis) and inclination, along with
// the radius of planets are stored in the following array
float eccentricity[] = {0,0.205,0.007,0.017,0.094,0.049,0.057,0.046,0.011,0.244};
float aphelion[] = {0.0,69.8,108.9,152.1,249.2,816.6,1514.5,3003.6,4545.7,7304.3};
float inclination[] = {0.0,7.0,3.4,0.0,1.9,1.3,2.5,0.8,1.8,17.2};

float bodyRadii[] = {0.0,0.5,0.7,0.7,0.6,1.7,1.5,3,3,2};
float orbitPeriod[] = {0.0,126720.0,323568.0,525888.0,989280.0,6380640.0,15475680.0,44048160.0,86112000.0,130446720.0};
float epoch[]={0.0,13627665.75,13630579.0,13922899.58,14241140.2,18535221.46,25301781.93,20140822.57,74731252.68,75518933.97};

int bodyColorR[] = {0,198,206,192,255,255,255,41,41,163};
int bodyColorG[] = {0,198,21,98,163,234,134,100,100,164};
int bodyColorB[] = {0,198,21,255,41,41,41,255,255,165};

// The parameters to define our pseudo-camera through which we view the scene
float camx = 0, camy = 0, camz = 0, scale = 1.0;

long currentTime;
int timeRate = 0;
int fastForward = 0;

boolean orbit = true;
int following = 0;

// This function runs once in the beginning of the program
void setup() {
  //size(832,624,P3D); // This specifies the size of the screen along with type of renderer (3D, in our case)
  fullScreen(P3D,1);
  stroke(255);  // The color with which we stroke is white in grayscale.
  
}

// This function runs in loop throughout the program
void draw() {
  incrTime();
  background(0, 0, 0); // We clear the background to black before drawing anything
  translate(width/2, height/2, 0); // We bring the origin of drawing area to the center of screen
    
  // We set the pseudo-camera into user-specified position
  scale(scale);
  rotateX(camx);
  rotateY(camy);
  rotateZ(camz);

  if(following > 0) followBody(following);  

  // We draw the sun at the center in white
  fill(255,255,255); 
  sphere(10);

  // This loop runs once for each planet in the array.
  for(int planet=1;planet<10;planet++) {
    pushMatrix();
    rotateY(radians(inclination[planet]));
    if(orbit) plotOrbit(planet); // This function plots the orbit of each planet
    plotBody(planet); // This function plots the body around the body
    popMatrix();
    
    
  }
  if (keyPressed && (key == 'Q' || key == 'q' || key == 'E' || key == 'e' 
      || key == 'A' || key == 'a' || key == 'D' || key == 'd' || key == 'W' || key == 'w' || key == 'S' || key == 's'
      || key == CODED)) keyPressed();
}

// This function changes the parameter of the camera using user's keyboard
void keyPressed() {
  if (key == 'Q' || key == 'q') {
    camz+=0.01;
  }
  else if (key == 'E' || key == 'e') {
    camz-=0.01;
  }
  else if(key == CODED) {
    if (keyCode == UP) {
      scale+=0.025;
    }
    else if (keyCode == DOWN) {
      if(scale>0.05) scale-=0.025;
    }
  }
  else if (key == 'A' || key == 'a') {
    camy-=0.01;
  }
  else if (key == 'D' || key == 'd') {
    camy+=0.01;
  }
  else if (key == 'W' || key == 'w') {
    camx+=0.01;
  }
  else if (key == 'S' || key == 's') {
    camx-=0.01;
  }
  else if (key == 'O' || key == 'o') {
    orbit = !orbit;
  }
  else if (key == 'L' || key == 'l') {
    timeRate = 0;
    fastForward = 0;
  }
  else if (key == 'M' || key == 'm') {
    if(timeRate<9) {
      timeRate++;
      fastForward = 1;
      incrTime();
    }
  }
  else if (key == 'N' || key == 'n') {
    if(timeRate>-9) {
      timeRate--;
      fastForward = 1;
      incrTime();
    }
  }
  else if (key == '1') {
    following = 1;
  }
  else if (key == '2') {
    following = 2;
  }
  else if (key == '3') {
    following = 3;
  }
  else if (key == '4') {
    following = 4;
  }
  else if (key == '5') {
    following = 5;
  }
  else if (key == '6') {
    following = 6;
  }
  else if (key == '7') {
    following = 7;
  }
  else if (key == '8') {
    following = 8;
  }
  else if (key == '9') {
    following = 9;
  }
  else if (key == '0') {
    following = 0;
  }
}
void followBody(int id) {
        float timeDiff = currentTime - epoch[id];
        if(timeDiff<0) timeDiff -= orbitPeriod[id];        
        float i = ((2*PI)/orbitPeriod[id])*timeDiff;
        float a = aphelion[id]/3;
        float e = eccentricity[id];
        float r = (a*(1-(e*e)))/(1-(e*cos(i)));
        float x = r*cos(i);
        float y = r*sin(i);
        rotateY(radians(inclination[id]));
        translate(-x,-y,0);
        
}

// This function plots planets
void plotBody(int id) {
        float timeDiff = currentTime - epoch[id];
        if(timeDiff<0) timeDiff-=orbitPeriod[id];        
        float i = ((2*PI)/orbitPeriod[id])*timeDiff;
        float a = aphelion[id]/3;
        float e = eccentricity[id];
        float r = (a*(1-(e*e)))/(1-(e*cos(i)));
        float x = r*cos(i);
        float y = r*sin(i);

        pushMatrix();
          translate(x, y);
          noStroke();
          fill(bodyColorR[id],bodyColorG[id],bodyColorB[id]);
          sphere(bodyRadii[id]*4);
          stroke(255);
        popMatrix();
        
}

// This plots the orbits of the planets
void plotOrbit(int id) {
    float circumference = 2*PI;
    float a = aphelion[id]/3;
    float e = eccentricity[id];
    float prevx = -1;
    float prevy = -1;
    for(float theta=0.0;theta<circumference;theta+=0.01) {
      float r = (a*(1-(e*e)))/(1-(e*cos(theta)));
      float x = r*cos(theta);
      float y = r*sin(theta);
      if(prevx == -1 && prevy == -1) {
        prevx = x;
        prevy = y;
      } else {
        line(prevx,prevy,x,y);
        prevx = x;
        prevy = y;
        
      }
    }
}

// This function updates the current time of the application
void incrTime() {
        Date d = new Date();
        long x = d.getTime()/1000;
        long appTime;
        switch(timeRate) {
                case 0:
                        if(fastForward==1) {
                                appTime = currentTime*60;
                                break;
                        } else if(fastForward==0) {
                                appTime = x;
                                currentTime = x/60;
                                break;
                        }
                case 1:
                        currentTime+=130;        
                        appTime = currentTime*60;
                        break;
                case 2:
                        currentTime+=400;        
                        appTime = currentTime*60;
                        break;
                case 3:
                        currentTime+=625;        
                        appTime = currentTime*60;
                        break;
                case 4:
                        currentTime+=1250;        
                        appTime = currentTime*60;
                        break;
                case 5:
                        currentTime+=2500;        
                        appTime = currentTime*60;
                        break;
                case 6:
                        currentTime+=5000;        
                        appTime = currentTime*60;
                        break;
                case 7:
                        currentTime+=10000;        
                        appTime = currentTime*60;
                        break;
                case 8:
                        currentTime+=20000;        
                        appTime = currentTime*60;
                        break;
                case 9:
                        currentTime+=40000;        
                        appTime = currentTime*60;
                        break;                
                case -1:
                        currentTime-=130;        
                        appTime = currentTime*60;
                        break;
                case -2:
                        currentTime-=400;        
                        appTime = currentTime*60;
                        break;
                case -3:
                        currentTime-=625;        
                        appTime = currentTime*60;
                        break;
                case -4:
                        currentTime-=1250;        
                        appTime = currentTime*60;
                        break;
                case -5:
                        currentTime-=2500;        
                        appTime = currentTime*60;
                        break;
                case -6:
                        currentTime-=5000;        
                        appTime = currentTime*60;
                        break;
                case -7:
                        currentTime-=10000;        
                        appTime = currentTime*60;
                        break;
                case -8:
                        currentTime-=20000;        
                        appTime = currentTime*60;
                        break;
                case -9:
                        currentTime-=40000;        
                        appTime = currentTime*60;
                        break;
        }
}
