//https://disipio.wordpress.com/2012/12/31/simulate-the-solar-system-with-processing/

import traer.physics.*;
float AU = 1.496e11; // m
// 20 * AU = width/2 // radius of the mini solar system
float G_N = 6.67e-11;
float m_sun = 1.989e30;
float m_venus = 4.8e24;
float m_earth = 5.9e24;
float m_mars = 6e23;
float m_jupiter = 1.9e27;
float m_saturn = 5e26;
float m_uranus = 8.26e25;
float m_neptune = 1e26;
// standard grav parameter mu=GM
float mu_solar_system = G_N * m_sun;
float mu_solar_system_reduced = mu_solar_system / m_earth;
float r_venus = 0.7;
float r_earth = 1.; //AU
float r_mars = 1.4;
float r_jupiter = 5.;
float r_saturn = 9.;
float r_uranus = 20.;
float r_neptune = 29;
ParticleSystem physics;
Particle sun;
Particle venus; //venus
Particle earth; //earth
Particle mars; //mars
Particle jupiter; //jupiter
Particle saturn; //saturn
Particle uranus; //uranus
Particle neptune; //neptune
void setup()
{
 size( 800, 800 );
 smooth();
 
 float r_0 = ( width/70 );
fill( 0 );
 ellipseMode( CENTER );
 
 println( "mu of the solar system " + mu_solar_system );
 println( "speed of earth at perihelion = " + sqrt(mu_solar_system/AU) + " m/s");
println(" width = " + width + " r_0 = " + r_0 );
 
 physics = new ParticleSystem( 0., 0. );
 physics.setIntegrator( ParticleSystem.RUNGE_KUTTA );
 
 sun = physics.makeParticle( m_sun/m_earth, width/2, height/2, 0 );
 sun.makeFixed();
 
 venus = physics.makeParticle( m_venus/m_earth, width/2 + r_0*r_venus, height/2, 0 );
 venus.velocity().set( 0, sqrt(mu_solar_system_reduced/r_venus/r_0), 0. );
 
 earth = physics.makeParticle( 1, width/2 + r_0*r_earth, height/2, 0 );
 earth.velocity().set( 0, sqrt(mu_solar_system_reduced/r_0), 0. );
 
 mars = physics.makeParticle( m_mars/m_earth, width/2 + r_0*r_mars , height/2, 0 );
 mars.velocity().set( 0, sqrt(mu_solar_system_reduced/r_mars/r_0), 0. );
 
 jupiter = physics.makeParticle( m_jupiter/m_earth, width/2 + r_0*r_jupiter , height/2, 0 );
 jupiter.velocity().set( 0, sqrt(mu_solar_system_reduced/r_jupiter/r_0), 0. );
 
 saturn = physics.makeParticle( m_saturn/m_earth, width/2 + r_0*r_saturn , height/2, 0 );
 saturn.velocity().set( 0, sqrt(mu_solar_system_reduced/r_saturn/r_0), 0. );
 
 uranus = physics.makeParticle( m_uranus/m_earth, width/2 + r_0*r_uranus , height/2, 0 );
 uranus.velocity().set( 0, sqrt(mu_solar_system_reduced/r_uranus/r_0), 0. );
 
 neptune = physics.makeParticle( m_neptune/m_earth, width/2 + r_0*r_neptune , height/2, 0 );
 neptune.velocity().set( 0, sqrt(mu_solar_system_reduced/r_neptune/r_0), 0. );
 
 
 println( "Start:" );
 //println( "vx = " + earth.velocity().x() + " vy = " + earth.velocity().y() );
 
 physics.makeAttraction( sun, earth, G_N, r_0/100 );
 physics.makeAttraction( sun, mars, G_N, r_0/100 );
 physics.makeAttraction( sun, jupiter, G_N, r_0/100 );
 physics.makeAttraction( sun, saturn, G_N, r_0/100 );
 physics.makeAttraction( sun, venus, G_N, r_0/100 );
 physics.makeAttraction( sun, uranus, G_N, r_0/100 );
 physics.makeAttraction( sun, neptune, G_N, r_0/100 );
physics.makeAttraction( earth, mars, G_N, r_0/100 );
 physics.makeAttraction( earth, jupiter, G_N, r_0/100 );
 physics.makeAttraction( earth, venus, G_N, r_0/100 );
 physics.makeAttraction( mars, venus, G_N, r_0/100 );
 physics.makeAttraction( mars, jupiter, G_N, r_0/100 );
 physics.makeAttraction( jupiter, saturn, G_N, r_0/100 );
 
 physics.makeAttraction( uranus, jupiter, G_N, r_0/100 );
 physics.makeAttraction( uranus, saturn, G_N, r_0/100 );
 
 physics.makeAttraction( neptune, jupiter, G_N, r_0/100 );
 physics.makeAttraction( neptune, saturn, G_N, r_0/100 );
}
void mousePressed()
{
 
}
void mouseDragged()
{
 
}
void mouseReleased()
{
 
}
void draw()
{
 println( "(position: x, y) = ( " + saturn.position().x() + ", " + saturn.position().y() + ")" + " (velocity: vx, vy) = ( " + saturn.velocity().x() + ", " + saturn.velocity().y() + ")");
 
 physics.tick(1000);
 
 background( 0 );
 
 color y = color( 255, 204, 0);
 fill(y);
 noStroke();
 ellipse( sun.position().x(), sun.position().y(), 12, 12 );
color g = color( 0, 200, 100);
 fill(g);
 ellipse( venus.position().x(), venus.position().y(), 5, 5 );
 
 color b = color( 0, 100, 230);
 fill(b);
 ellipse( earth.position().x(), earth.position().y(), 5, 5 );
 
 color r = color( 255, 0, 0);
 fill(r);
 ellipse( mars.position().x(), mars.position().y(), 3, 3 );
 
 color j = color( 255, 150, 100);
 fill(j);
 ellipse( jupiter.position().x(), jupiter.position().y(), 10, 10 ); 
 
 color s = color( 100, 110, 100);
 fill(s);
 ellipse( saturn.position().x(), saturn.position().y(), 15, 4 ); 
 color w = color( 200, 150, 100 );
 fill(w); 
 ellipse( saturn.position().x(), saturn.position().y(), 8, 8 );
 
 color b2 = color( 0, 200, 200);
 fill(b2);
 ellipse( uranus.position().x(), uranus.position().y(), 8, 8 );
 
 color b3 = color( 0, 0, 250);
 fill(b3);
 ellipse( neptune.position().x(), neptune.position().y(), 8, 8 );
 
}
